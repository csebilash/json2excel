var express = require("express");
var exportToExcel = require('export-to-excel');
var _ = require("underscore");
var app = express();
var jdata = require('./sale.json');

app.get('/',function(req,res){
	var customerInfo = [],i;
	var saleData = jdata.sales;
	for( i=0;i<saleData.length;i++){
		customerInfo.push({Id: (i+1), Name : saleData[i].customerName , Mobile: saleData[i].phoneNumber, Address : saleData[i].address})
	}
	customerInfo = _.uniq(customerInfo, 'Mobile');
	if(i == saleData.length){
		console.log("Total Data : "+customerInfo.length);
		    exportToExcel.exportXLSX({
		    filename: 'createOutput',
		    sheetname: 'Customer Data',
		    title: [
		    	{
		            "fieldName": "Id",
		            "displayName": "Id",
		            "cellWidth": 30
		        },
		        {
		            "fieldName": "Name",
		            "displayName": "Name",
		            "cellWidth": 30
		        },
		        {
		            "fieldName": "Mobile",
		            "displayName": "Mobile No",
		            "cellWidth": 15,
		            "type": "string"
		        },
		        {
		            "fieldName": "Address",
		            "displayName": "Address",
		            "cellWidth": 15
		        }
		    ],
		    data: customerInfo
		});
	}


})

app.listen(3000,function(){
	console.log("App is running on port 3000");
})